package com.freecharge.aop.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
@Order(1)
public class LoggingAspect {

    Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

    @Autowired
    HttpServletRequest httpServletRequest;

    /*
    Advice, Pointcut,
    Before -> Before calling the method apply this advice
    AfterReturning -> After the completion of the method successfully
    AfterThrowing -> Method throws Exception
    After
    Around

    PointCut Designator
    identify method
    Target Object -> object on which advice is applied (business logic)
    Proxy object -> duplicate object (applying aspects)
     */

    @Before(value = "execution(* com.freecharge.aop.controller.*.*(..))")
    public void beforeAdvice(JoinPoint joinPoint){
        logger.info("Before Advice called for the method : " + joinPoint.getSignature());
        logger.info("Requested End Point : " + httpServletRequest.getRequestURL());
    }

   /* @AfterReturning(value = "execution(* com.freecharge.aop.service.*.*(..))", returning = "price")
    public void afterReturning(JoinPoint joinPoint, Double price){
        logger.info("After Returning advice applied for the method : " + joinPoint.getSignature());
        logger.info("price : " + price);
    }

    @AfterThrowing(value = "execution(* com.freecharge.aop.service.*.*(..))",throwing = "exception")
    public void afterThrowing(JoinPoint joinPoint, Throwable exception){
        logger.info("Exception Occured in : " + joinPoint.getSignature());
        logger.error("exception", exception.getMessage());
    }
*/

    @After(value = "execution(* com.freecharge.aop.service.*.*(..))")
    public void afterFinallyAdvice(JoinPoint joinPoint){
        /*
        HttpSErvletReqeust, HttpServletResponse
         */
        logger.info("After Finally applied .." + joinPoint.getSignature());
    }

    /*
    Around advice
    before the calling method -> throw an exception
    -> actual target object
    response data
    manipulate response
     */

    @Around(value = "execution(* com.freecharge.aop.service.*.*(..))")
    public Object aroundAdvice(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        logger.info("Around advice is called : " + proceedingJoinPoint.getSignature());
        logger.info("Arguments Passed for the method :" + proceedingJoinPoint.getArgs()[0]);
        Object response = proceedingJoinPoint.proceed(new Object[]{10});
        logger.info("Actual Response {}", response);
        return 11111.11;
    }

}
