package com.freecharge.aop.aspect;

import com.freecharge.aop.exception.AuthException;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
@Order(0)
public class SecurityAspect {

    @Autowired
    HttpServletRequest httpServletRequest;
    private Logger logger = LoggerFactory.getLogger(SecurityAspect.class);

    @Before(value = "execution(* com.freecharge.aop.controller.*.*(..))")
    public void securityBeforeAdvice(JoinPoint joinPoint) throws AuthException {
        logger.info("Security Before advice ::" + joinPoint.getSignature());
        String authorization = httpServletRequest.getHeader("Authorization");
        if(authorization == null)
            throw new AuthException("No Authorization Header");
        //Authorization: Bearer <Token>
        String bearerToken = authorization.split(" ")[1];
        String bearerTokenValue = "asdfa23423asdfasfd";
        if(!bearerToken.equals(bearerTokenValue))
            throw new AuthException("Invalid Bearer Token");
    }
}
