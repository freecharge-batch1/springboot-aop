package com.freecharge.aop.controller;

import com.freecharge.aop.exception.ProductNotFoundException;
import com.freecharge.aop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1")
public class ProductController {

    @Autowired
    ProductService productService;
 /* com.freecharge.aop.controller.ProductController.getProduct()*/
    @GetMapping("/products")
    public ResponseEntity<?> getProducts(){
        return new ResponseEntity<>("Products List....", HttpStatus.OK);
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<?> getProductPrice(@PathVariable("id") int id) throws ProductNotFoundException {
        return new ResponseEntity<>(this.productService.getProductPrice(id), HttpStatus.OK);
    }


}
