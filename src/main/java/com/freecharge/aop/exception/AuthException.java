package com.freecharge.aop.exception;

public class AuthException extends Exception{

    public AuthException(String message) {
        super(message);
    }
}
