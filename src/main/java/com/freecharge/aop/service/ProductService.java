package com.freecharge.aop.service;

import com.freecharge.aop.exception.ProductNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class ProductService {

    public double getProductPrice(int id) throws ProductNotFoundException {
        if(id == 3)
            throw new ProductNotFoundException("Product Not found with id: " + id);
        return 34543.345;
    }
}
